let me = {
       name: "Satyendra Singh Yadav",
       thisInArrow:() => {
       console.log(`My name is ${name}`);
    },
    thisInRegular(){
      console.log(`My name is ${this.name}`);
    }
  };
  me.thisInArrow();
  me.thisInRegular();

const students = [
    {name:"Harry", subject:"JavaScript"},
    {name:"Satyendra", subject:"ReactJs"}
]

const enrolledStudent = (student, callback) =>{
  setTimeout(()=>{
     students.push(student);
     console.log("Student Has been Enrolled");
     callback();
  },3000)
}

const getStudents=() =>{
  setTimeout(()=>{
    let str = "";
    students.forEach(student =>{
      console.log("Student Has Been Fatched");
      console.log(`${student.name},${student.subject}`);

    })
  },1000)
}
let newStudent = {name:"sunny", subject:"Python"};

enrolledStudent(newStudent, getStudents);
